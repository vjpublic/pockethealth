1. For this assignment, the backend is written in Golang. Please go here, and follow the appropriate instructions to set up the environment on your machine.

2. Please unzip the starter code here. You will find the source code for the server (httpserver.go) along with some additional folders.

3. Please go ahead and compile the server. It is configured to run on your localhost, on port 8080. Using any browser, you may wish to visit http://localhost:8080 once you execute the binary. 

4. For production, we'd like to modify the server to run on port 80. Please make the appropriate modification. [Bonus Points: Secure the endpoint and modify it to use TLS, feel free to use a self-signed certificate.]

5. We're considering allowing anyone to subscribe for PocketHealth news. This task will give you a flavour of the approach. 

Please modify the server to add a path, "/subscribe". When a user visits this page, they should be presented with a form allowing them to provide the following information:

    Name
    Email
    Telephone Number

Please use the default template (tmpl/default.html) as a starting point for the form. The form should POST to the path "/subscribeconfirm".

6. After some time, we decide that we want to capture the user's favourite colour. Please extend the form and confirmation page to collect and confirm the user's favourite colour. 

7. You may wish to perform some basic input sanitization in the front end (and/or the backend). If you wish, go ahead and do so.

8. Now, we wish to actually store these subscriptions in a database. In this task, create a simple SQLite database with a table with appropriate schema to store the information we are collecting. Modify the /subscribeconfirm handler to also write the submitted data into the database. 

9. Now, please create a simple handler (/v1/internal/subscriptions/list) that returns a JSON formatted list of subscribed users.

10. Finally, we're ready to deploy. Please create a Docker container for your solution.

11. Please prepare a formal merge request (pull request) as if you were preparing to deploy the changes in this assignment. You may email this to us along with the solution in the next step.

12. Please provide a zip of your solution, and email it back to this address by Sunday February 7 at 1:00PM. If you are unable to attach the zip to an email, please feel free to share a Google Drive or Dropbox link.

