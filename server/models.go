package server

import (
	"database/sql"
	"log"
)

type SubscribeConfirmPage struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Tel   string `json:"tel"`
}

// Referenced
// https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/05.3.html
func (obj *SubscribeConfirmPage) Save(db *sql.DB) (error, int64) {
	stmt, err := db.Prepare("INSERT INTO userinfo(name, email, tel) values(?,?,?)")
	if err != nil {
		log.Println(err)
		return err, -1
	}

	res, err := stmt.Exec(obj.Name, obj.Email, obj.Tel)
	if err != nil {
		log.Println(err)
		return err, -1
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
		return err, -1
	}

	return nil, id
}

func (obj *SubscribeConfirmPage) GetAll(db *sql.DB) ([]*SubscribeConfirmPage, error) {
	var objs []*SubscribeConfirmPage
	var tmpObj SubscribeConfirmPage

	rows, err := db.Query("SELECT * FROM userinfo")
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&tmpObj.Name, &tmpObj.Email, &tmpObj.Tel)
		objs = append(objs, &tmpObj)
	}

	return objs, nil
}
