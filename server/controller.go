package server

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
)

func (obj *Server) handleSubscribe(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles(obj.wd + "tmpl/subscribe.html")
	t.Execute(w, "")
}

func (obj *Server) handleSubscribeConfirm(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		subscribeConfirmPage := SubscribeConfirmPage{
			Name:  r.FormValue("name"),
			Email: r.FormValue("email"),
			Tel:   r.FormValue("tel"),
		}
		err, _ := subscribeConfirmPage.Save(obj.GetDatabase())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to save object."))
			return
		}

		t, _ := template.ParseFiles(obj.wd + "tmpl/subscribeconfirm.html")
		t.Execute(w, subscribeConfirmPage)
	} else {
		w.Header().Set("Allow", "POST")
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

func (obj *Server) handleSubscribers(w http.ResponseWriter, r *http.Request) {
	var modalObj *SubscribeConfirmPage

	objs, err := modalObj.GetAll(obj.GetDatabase())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to find objects."))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(objs)

}

func (obj *Server) handleDefault(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles(obj.wd + "tmpl/default.html")
	if err != nil {
		log.Println(err)
		return
	}

	t.Execute(w, "")
}
