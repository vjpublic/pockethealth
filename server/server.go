package server

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

// Server is the struct for server
type Server struct {
	database *sql.DB
	mux      *http.ServeMux
	wd       string
}

// GetDatabase returns the database
func (obj *Server) GetDatabase() *sql.DB {
	return obj.database
}

func (obj *Server) SetWorkingDirForTemplates() error {
	wd, err := os.Getwd()
	if err != nil {
		log.Println(err)
		return err
	}

	obj.wd = wd
	return nil
}

func (obj *Server) SetDatabase() error {
	db, err := sql.Open("sqlite3", "./tmp.db")
	if err != nil {
		log.Println(err)
		return err
	}

	obj.database = db
	_, err = db.Exec("CREATE TABLE `userinfo` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `tel` VARCHAR(255))")
	if err != nil {
		log.Println(err)
	}

	return nil
}

// Run runs the server
func Run() {
	var serverObj Server
	// var databaseObj MockDB

	// serverObj.database = &databaseObj
	serverObj.mux = http.NewServeMux()

	serverObj.SetWorkingDirForTemplates()
	serverObj.initRoutes()
	serverObj.SetDatabase()

	server := &http.Server{Addr: ":" + os.Getenv("PORT_PRODUCTION"), Handler: serverObj.mux}
	server.ListenAndServe()
}
