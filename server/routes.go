package server

import "net/http"

func (obj *Server) initRoutes() {
	obj.mux.HandleFunc("/", obj.handleDefault)
	obj.mux.HandleFunc("/subscribe", obj.handleSubscribe)
	obj.mux.HandleFunc("/subscribers", obj.handleSubscribers)
	obj.mux.HandleFunc("/subscribeconfirm", obj.handleSubscribeConfirm)
	obj.mux.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))
}
