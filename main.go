package main

import (
	"log"

	"github.com/joho/godotenv"

	"gitlab.com/vjpublic/pockethealth/server"
)

func main() {
	var err error

	err = godotenv.Load()
	if err != nil {
		log.Fatal("Failed to load environment variables")
	} else {
		log.Println("Environment Variables Loaded")
	}

	server.Run()
}
