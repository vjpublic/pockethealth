module gitlab.com/vjpublic/pockethealth

go 1.15

require (
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-sqlite3 v1.14.6
)
