FROM golang:1.14.3-alpine AS builder

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN apk --no-cache add make git gcc libtool musl-dev ca-certificates dumb-init 

COPY go.mod .
COPY go.sum .
RUN go mod download

RUN CGO_ENABLED=1 GOOS=linux go build -a -o main .

FROM alpine:3.11.3
COPY --from=builder /app/tmpl /tmpl/
COPY --from=builder /app/assets /assets/
COPY --from=builder /app/main .
COPY --from=builder /app/.env .

RUN ls

# executable
ENTRYPOINT [ "./main" ]